from django.shortcuts import render_to_response, render
from django.template.context import RequestContext
from core.models import Cliente, Linha, Rota, Itinerario, Usuario
from math import ceil
from django.db import transaction
import json
from django.http.response import HttpResponse, JsonResponse,HttpResponseRedirect
import re
from django.contrib.auth import authenticate, login
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.views import logout
from django.contrib.auth.decorators import login_required

def crud(request):
    return render_to_response('core/baseItens/baseCRUD.html',{})


def listcrud(request):
    return render_to_response('core/baseItens/baseListCRUD.html',{})
    
def register(request,*args, **kwargs):
    result = {}
    messages = []
    
    if request.method == 'POST':
        usuario = Usuario()
        user = User.objects.create_user(request.POST['userName'],request.POST['email'],request.POST['password'])
        usuario.user_id = user.id
        usuario.save()

        user = authenticate(username=request.POST['userName'],password=request.POST['password'])
        login(request, user)                
        messages.append('Cadastro realizado com sucesso!')
        messages.append('Seja Bem-vindo ao EasyBus')
        result['success'] = True
        result['messages'] = messages
        result['resultUrl'] = 'manutCliente'
        return JsonResponse(result)
    else:
        return home(request)
    
def loginUser(request,*args, **kwargs):
    
    result = {}
    messages = []
    
    if request.method == 'POST':

        userEntry = request.POST['userEntry']

        print(userEntry)
        userPassword = request.POST['userPassword']        
        user = authenticate(username=userEntry,password=userPassword)
        if user is None:
            userByEmail = User.objects.get(email=userEntry)
            user = authenticate(username=userByEmail.username,password=userPassword) 
        if user is not None:
            if user.is_active:
                login(request,user)
                messages.append('Login efetuado!')
                result['success'] = True
                result['messages'] = messages
                result['resultUrl'] = 'manutCliente'
                return JsonResponse(result)
            else:
                messages.append('Usuário ainda não foi ativado! \n Verifique seu email')
        else:
            messages.append('Usuário ou senha inválido!')
            
        result['success'] = False
        result['messages'] = messages
        return JsonResponse(result)
    else:
        return home(request)

def logoutUser(request,*args, **kwargs):
    auth.logout(request)
    return HttpResponseRedirect('/')
    
def isLogged(request):
    print('canal')
    print(request.user.is_authenticated())
    print('canal2')
    
    if not request.user.is_authenticated():
        print('retorna')
        return HttpResponseRedirect('/')
    
def home(request,*args, **kwargs):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/manutCliente/')
    else:
        return render(request, 'core/login/startPage.html')

@login_required
def listCliente(request):    
    listaLabels = ["Código", "Nome","CNPJ"]
    result = {} 
    result['listaItens'] = Cliente.objects.all()
    result['listaLabels'] = listaLabels
    result['pageTitle'] = 'Manutenção de Clientes'
    result['pageIcon'] = 'fa-group'
    result['title'] = 'Cliente'   
    result['manutUrl'] = "/manutCliente/"
    result['searchUrl'] = 'buscaClientes'
    
    result = paginationProperties(result)
        
    return render_to_response('core/cliente/listCliente.html',result,context_instance=RequestContext(request))

def manutCliente(request,crudOption,ID=0):
    
    isLogged(request)
    
    context = getCRUDProperties(crudOption)   
    context['title'] = 'Cliente'
    context['crudListUrl'] = '/manutCliente/'
    
    cliente = Cliente()
    
    if(crudOption != 'ADD'):
        cliente = Cliente.objects.get(id=ID) 
        context['cliente'] = cliente
        
    if request.method == 'POST':
        
        success = False
        messages = []
        
        if(crudOption == 'RMV'):
            cliente.delete()
            messages.append('Cliente removido com sucesso!')
            success = True
        else:
            cliente.nome = request.POST['nome']
            cliente.cnpj = request.POST['cnpj']
            cliente.email = request.POST['email']
            cliente.telefone = request.POST['telefone']
            messages = validaCliente(cliente)
            
            if (len(messages) == 0):
                success = True
                messages.append('Cliente ' + ('adicionado' if crudOption == 'ADD' else 'modificado') + ' com sucesso!')
                cliente.save()
                                
        result = {}
        result['messages'] = messages #json.dumps(messages, ensure_ascii=False)
        result['success'] = success
        return JsonResponse(result)
                
    return render_to_response('core/cliente/manutCliente.html',context, context_instance=RequestContext(request))

@login_required
def listLinha(request):

    isLogged(request)

    listaLabels = ["Código", "Cliente","Linha"]
    result = {}
    result['listaItens'] = Linha.objects.all()
    result['listaLabels'] = listaLabels
    result['pageTitle'] = 'Manutenção das Linhas de Ônibus'
    result['pageIcon'] = 'fa-road'
    result['title'] = 'Linhas de Ônibus'   
    result['manutUrl'] = "/manutLinha/"
    result['searchUrl'] = "buscaLinhas"
    
    result = paginationProperties(result)
        
    return render_to_response('core/linha/listLinha.html',result,context_instance=RequestContext(request))

def manutLinha(request,crudOption,ID=0):
    
    isLogged(request)
    
    context = getCRUDProperties(crudOption)    
    
    context['title'] = 'Linhas de Ônibus'
    context['crudListUrl'] = '/manutLinha/'
    
    linha = Linha()
    
    if crudOption != 'ADD':
        linha = Linha.objects.get(id=ID) 
        
    context['linha'] = linha
    context['clientes'] = Cliente.objects.all()                   
        
    if request.method == 'POST':
        
        success = False
        messages = []
                
        if(crudOption == 'RMV'):
            linha.delete()
            success = True
            messages.append('Linha removida com sucesso!') 
        else:            
            linha.id_cliente =  Cliente.objects.get(id=request.POST['cliente'])
            linha.nome_linha = request.POST['nomeLinha']
            linha.origem = request.POST['origem']
            linha.destino = request.POST['destino']
            
            if(linha.id_cliente != None):
                success = True
                messages.append('Linha ' + ('adicionada' if crudOption == 'ADD' else 'modificada') + ' com sucesso!')
                linha.save()
            else:
                messages.append('Cliente selecionado inválido!')
                
        result = {}
        result['messages'] = messages #json.dumps(messages, ensure_ascii=False)
        result['success'] = success
        return JsonResponse(result)
            
    return render_to_response('core/linha/manutLinha.html',context, context_instance=RequestContext(request))

def listRota(request):

    isLogged(request)

    listaLabels = ["Código", "Cliente","Linha","Saida","Chegada","Estudante","Necessidades Especiais","Feriados"]
    result = {}
    result['listaItens'] = Rota.objects.all()
    result['listaLabels'] = listaLabels
    result['pageTitle'] = 'Manutenção das Rotas de Ônibus'
    result['pageIcon'] = 'fa-map'
    result['title'] = 'Rotas de Ônibus'   
    result['manutUrl'] = "/manutRota/"
    result['searchUrl'] = 'buscaRotas'
    
    result = paginationProperties(result)
        
    return render_to_response('core/rota/listRota.html',result,context_instance=RequestContext(request))

def manutRota(request,crudOption,ID=0):
    
    isLogged(request)
    
    context = getCRUDProperties(crudOption)    
    
    context['title'] = 'Rotas de Ônibus'
    context['crudListUrl'] = '/manutRota/'
    
    rota = Rota()
    context['clientes'] = Cliente.objects.all()                   
    
    if crudOption != 'ADD':
        rota = Rota.objects.get(id=ID)
        context['linhas'] = Linha.objects.all().filter(id_cliente=rota.id_linha.id_cliente)
    else:
        clientes = context['clientes']
        context['linhas'] = Linha.objects.all().filter(id_cliente=clientes[0].id)
        
    context['rota'] = rota
        
    if request.method == 'POST':
        
        success = False
        messages = []        
                
        if(crudOption == 'RMV'):
            rota.delete()
            success = True
            messages.append('Rota removida com sucesso!')
        else:                            
            rota.id_linha =  Linha.objects.get(id=request.POST['linha'])
            rota.horario_saida = request.POST['saida']
            rota.horario_chegada = request.POST['chegada']
            rota.estudante = returnMysqlBooleanValue(request.POST,'estudante')
            rota.especiais = returnMysqlBooleanValue(request.POST,'especiais')
            rota.domingo = returnMysqlBooleanValue(request.POST,'domingo')
            rota.segunda = returnMysqlBooleanValue(request.POST,'segunda')
            rota.terca = returnMysqlBooleanValue(request.POST,'terca')
            rota.quarta = returnMysqlBooleanValue(request.POST,'quarta')
            rota.quinta = returnMysqlBooleanValue(request.POST,'quinta')
            rota.sexta = returnMysqlBooleanValue(request.POST,'sexta')
            rota.sabado = returnMysqlBooleanValue(request.POST,'sabado')
            rota.feriado = returnMysqlBooleanValue(request.POST,'feriado')
            
            if(rota.id_linha != None):
                success = True
                rota.save()
                messages.append('Rota ' + ('adicionada' if crudOption == 'ADD' else 'modificada') + ' com sucesso!')
            else:
                messages.append('Linha selecionada inválida!')
           
        result = {}
        result['messages'] = messages #json.dumps(messages, ensure_ascii=False)
        result['success'] = success
        return JsonResponse(result)
                
    return render_to_response('core/rota/manutRota.html',context, context_instance=RequestContext(request))

def listItinerario(request):

    isLogged(request)

    listaLabels = ["Cliente","Linha","Rota","Horario","Sequência","Parada","Local"]
    result = {}
    result['listaItens'] = Itinerario.objects.all()
    result['listaLabels'] = listaLabels
    result['pageTitle'] = 'Manutenção de Itinerario'
    result['pageIcon'] = 'fa-street-view'
    result['title'] = 'Itinerario'   
    result['manutUrl'] = "/manutItinerario/"
    result['searchUrl'] = "buscaItinerarios"
    
    result = paginationProperties(result)
        
    return render_to_response('core/itinerario/listItinerario.html',result,context_instance=RequestContext(request))

def manutItinerario(request,crudOption,ID=0):
    
    isLogged(request)
    
    context = getCRUDProperties(crudOption)    
    
    context['title'] = 'Itinerario'
    context['crudListUrl'] = '/manutItinerario/'
    
    clientes =  Cliente.objects.all() 
    linhas = []
    rotas = []
    itinerario = Itinerario()
    
    
    if crudOption != 'ADD':
        itinerario = Itinerario.objects.get(id=ID)
        linhas = Linha.objects.all().filter(id_cliente=itinerario.id_rota.id_linha.id_cliente) 
        rotas = Rota.objects.all().filter(id_linha=itinerario.id_rota.id_linha)
        
    else:
        linhas = Linha.objects.all().filter(id_cliente=clientes[0].id)
        rotas = Rota.objects.all().filter(id_linha=linhas[0].id)            
        itinerario.sequencia = retornaProxSequencia(rotas[0])    
            
    context['clientes'] = clientes  
    context['linhas'] = linhas          
    context['rotas'] = rotas
    context['itinerario'] = itinerario
        
    if request.method == 'POST':
        
        success = False
        messages = []        
                
        if(crudOption == 'RMV'):
            itinerario.delete()
            success = True
            messages.append('Linha removida com sucesso!')
        else:    
            itinerario.id_rota =  Rota.objects.get(id=request.POST['rota'])
            itinerario.sequencia = request.POST['sequencia']
            itinerario.horario = request.POST['horario']
            itinerario.local = request.POST['local']
            itinerario.latitude = request.POST['latitude']
            itinerario.longitude = request.POST['longitude']
            itinerario.parada = returnMysqlBooleanValue(request.POST,'parada')
            
            if(itinerario.id_rota != None):
                success = True
                itinerario.save()
                messages.append('Itinerario ' + ('adicionado' if crudOption == 'ADD' else 'modificado') + ' com sucesso!')
            else:
                messages.append('Rota selecionada inválida!')
            
        result = {}
        result['messages'] = messages #json.dumps(messages, ensure_ascii=False)
        result['success'] = success
        return JsonResponse(result)
            
    return render_to_response('core/itinerario/manutItinerario.html',context, context_instance=RequestContext(request))

def retornaProxSequencia(rota):
    sequencia = 1
    
    for item in Itinerario.objects.all().filter(id_rota=rota.id):
        if sequencia < item.sequencia:
            sequencia = item.sequencia
    return sequencia + 1  

def listTrajeto(request):

    isLogged(request)

    listaLabels = ["Código", "Cliente","Linha","Saida","Chegada","Estudante","Necessidades Especiais","Feriados"]
    result = {}
    
    itinerarios = Itinerario.objects.all()
    rotas = []
    
    for itinerario in itinerarios:
        if itinerario.id_rota not in rotas:
            rotas.append(Rota.objects.get(id=itinerario.id))
    
    result['listaItens'] = rotas
    result['listaLabels'] = listaLabels
    result['pageTitle'] = 'Manutenção dos Trajetos de Ônibus'
    result['pageIcon'] = 'fa-random'
    result['title'] = 'Trajetos de Ônibus'   
    
    result = paginationProperties(result)
        
    return render_to_response('core/trajeto/listTrajeto.html',result,context_instance=RequestContext(request))

@transaction.atomic
def buscaClientes(request,*args, **kwargs):
    
    if (request.method == 'POST'):
        x = request.POST['client_response']        
        mapQuery = json.loads(x)        
        
        clientes = getClientesByQuery(mapQuery)         
        html = []
        
        if(mapQuery['tipoRetorno'] == 'table'):
            for cliente in clientes:
                html.append('<tr>' +
                                '<th width="50">' + str(cliente.id) + '</th>' +
                                '<th>' + str(cliente.nome)  + '</th>' +
                                '<th>' + str(cliente.cnpj) + '</th>' +
                                returnActionsTableRow('/manutCliente/', cliente.id) +
                            '</tr>)')
        else:    
            for cliente in clientes:
                html.append('<option value="' + str(cliente.id) + '"> '  + str(cliente.id) + ' - ' + cliente.nome + '  </option>')     
        
        a = json.dumps(html)        
        return HttpResponse(a, content_type='application/json')  
   
@transaction.atomic
def buscaLinhas(request,*args, **kwargs):
    
    if (request.method == 'POST'):
        x = request.POST['client_response']        
        mapQuery = json.loads(x)    
        
        print(mapQuery)
            
        linhas = getLinhasByQuery(mapQuery) 
                
        html = []
        
        if(mapQuery['tipoRetorno'] == 'table'):
            for linha in linhas:
                html.append('<tr>' +
                                '<th width="50">' + str(linha.id) + '</th>' +
                                '<th>' + str(linha.id_cliente.id) + ' - ' + str(linha.id_cliente.nome) + '</th>' +
                                '<th>' + str(linha.nome_linha) + '</th>' +
                                returnActionsTableRow('/manutLinha/', linha.id) +
                            '</tr>)')
        else:
            for linha in linhas:
                html.append('<option value="' + str(linha.id) + '"> '  + str(linha.id) + ' - ' + linha.nome_linha + '  </option>')
        
        a = json.dumps(html)        
        return HttpResponse(a, content_type='application/json')  
    
@transaction.atomic
def buscaRotas(request,*args, **kwargs):
    
    if (request.method == 'POST'):
        x = request.POST['client_response']        
        mapQuery = json.loads(x)        
        
        rotas = getRotasByQuery(mapQuery)
        
        html = []
        if(mapQuery['tipoRetorno'] == 'table'):
            for rota in rotas:
                html.append('<tr>' +
                                '<th width="50">' + str(rota.id) + '</th>' +
                                '<th>' + str(rota.id_linha.id_cliente.id) + ' - ' + str(rota.id_linha.id_cliente.nome) + '</th>' +
                                '<th>' + str(rota.id_linha.id) + ' - ' + str(rota.id_linha.nome_linha) + '</th>' +
                                '<th>' + str(rota.horario_saida) + '</th>' +
                                '<th>' + str(rota.horario_chegada) + '</th>' +
                                '<th>' + ('Sim' if rota.estudante == 1  else 'Não') + '</th>' +
                                '<th>' + ('Sim' if rota.especiais == 1  else 'Não') + '</th>' +
                                '<th>' + ('Sim' if rota.feriado == 1  else 'Não') + '</th>' +
                                returnActionsTableRow('/manutRota/', rota.id) +
                            '</tr>)')
        else:
            for rota in rotas:
                html.append('<option value="' + str(rota.id) + '"> Saída ' + rota.horario_saida + ' - Chegada ' + rota.horario_chegada + '  </option>')
            
        a = json.dumps(html)        
        return HttpResponse(a, content_type='application/json')  
    
@transaction.atomic
def buscaItinerarios(request,*args, **kwargs):
    
    if (request.method == 'POST'):
        x = request.POST['client_response']        
        mapQuery = json.loads(x)        
        
        itinerarios = getItinerariosByQuery(mapQuery)
        
        retorno = []
        if('tipoRetorno' in mapQuery and mapQuery['tipoRetorno'] == 'table'):
            for itinerario in itinerarios:
                retorno.append('<tr>' +
                                '<th>' + str(itinerario.id_rota.id_linha.id_cliente.id) + ' - ' + str(itinerario.id_rota.id_linha.id_cliente.nome) + '</th>' +
                                '<th>' + str(itinerario.id_rota.id_linha.id) + ' - ' + str(itinerario.id_rota.id_linha.nome_linha) + '</th>' +
                                '<th>' + str(itinerario.id_rota.id) + ' - ' + str(itinerario.id_rota.horario_saida) + ' - ' + str(itinerario.id_rota.horario_chegada) + '</th>' +
                                '<th>' + str(itinerario.horario) + '</th>' +
                                '<th>' + str(itinerario.sequencia) + '</th>' +
                                '<th>' + str(itinerario.local) + '</th>' +
                                returnActionsTableRow('/manutRota/', itinerario.id) +
                            '</tr>)')
        
        else:
            direcoes = Itinerario.objects.all().filter(id_rota=mapQuery['id_rota'])
            direcoes = sorted(direcoes, key=lambda direcoes: direcoes.sequencia);     
                   
            for itinerario in direcoes:
                direcao = [itinerario.sequencia, itinerario.latitude, itinerario.longitude,True if itinerario.parada else False,itinerario.local,itinerario.horario]
                
                print()
                print('canaleto direcoes')
                print(direcao)
                print()
                
                retorno.append(direcao)
                
            
        a = json.dumps(retorno)
        
        return HttpResponse(a, content_type='application/json')  

def returnActionsTableRow(manutUrl,itemId):
    return ('<td class="actions" width="200">' +
                '<a class="btn btn-success btn-xs" href="' + manutUrl + 'VIEW/' + str(itemId) + '">Visualizar</a>' +
                '<a class="btn btn-warning btn-xs" href="' + manutUrl + 'MOD/' + str(itemId) + '">Editar</a>' +
                '<a class="btn btn-danger btn-xs"  href="' + manutUrl + 'RMV/' + str(itemId) + '">Excluir</a>' +
            '</td>')

def getCRUDProperties(option):
    if option == 'ADD' or option == 'MOD':
        return addAndModProperties(option)
    else: return viewAndRemoveProperties(option)   
    
def paginationProperties(result):    
    result['qtdPaginas'] = ceil(len(result['listaItens']) / 10)
    result['loopPaginas'] = range(1,ceil(len(result['listaItens']) / 10) +1)
    return result;
    
def viewAndRemoveProperties(option):
    properties = {}
    properties['crudFieldsProperties'] = 'disabled'
    properties['saveButtonProperties'] = 'style="display:none;"'
    properties['returnButtonLabel'] = "Cancelar"
    
    if option == 'RMV':
        properties['CRUDTitle'] = 'Remover'
    else: 
        properties['CRUDTitle'] = 'Detalhar'
        properties['returnButtonLabel'] = "Voltar"
        properties['removeButtonProperties'] = 'style="display:none;"'
        
    return properties;

def addAndModProperties(option):
    properties = {}
    properties['removeButtonProperties'] = 'style="display:none;"'
    properties['returnButtonLabel'] = "Cancelar"
    
    if option == 'ADD':
        properties['CRUDTitle'] = 'Adicionar'
    else: properties['CRUDTitle'] = 'Modificar'
    
    return properties;

def returnMysqlBooleanValue(requestPostValue,value):
    if value in requestPostValue and requestPostValue[value] == 'on':
        return 1
    return 0

def getClientesByQuery(mapQuery):
    clientes = Cliente.objects.all()
        
    if('id_cliente' in mapQuery):
        clientes = clientes.filter(id=mapQuery['id_cliente'])
        
    if('nome_cliente' in mapQuery):
        clientes = clientes.filter(nome__icontains=mapQuery['nome_cliente'])
    
    if('cnpj_cliente' in mapQuery):
        clientes = clientes.filter(cnpj=mapQuery['cnpj_cliente'])
        
    return clientes

def getLinhasByQuery(mapQuery):
    
    linhas = Linha.objects.all()
    
    if('id_cliente' in mapQuery
       or 'nome_cliente' in mapQuery):
        novasLinhas = []
        for cliente in getClientesByQuery(mapQuery):
            novasLinhas.extend(linhas.filter(id_cliente=cliente.id))
        linhas = novasLinhas
        
    if('id_linha' in mapQuery):
        linhas = linhas.filter(id=mapQuery['id_linha'])
    
    if('nome_linha' in mapQuery):
        linhas = linhas.filter(nome_linha__icontains=mapQuery['nome_linha'])
        
    return linhas
    
def getRotasByQuery(mapQuery):
    
    rotas = Rota.objects.all()
    
    if('id_cliente' in mapQuery
       or 'nome_cliente' in mapQuery
       or 'id_linhas' in mapQuery
       or 'nome_linha' in mapQuery):
        novasRotas = []
        for linha in getLinhasByQuery(mapQuery):
            novasRotas.extend(rotas.filter(id_linha=linha.id))
        rotas = novasRotas 
        
    if('id_rota' in mapQuery):
        rotas = rotas.filter(id=mapQuery['id_rota'])
    
    if('estudantes' in mapQuery):
        rotas = rotas.filter(estudante=1)
        
    if('especiais' in mapQuery):
        rotas = rotas.filter(especiais=1)
        
    if('feriado' in mapQuery):
        rotas = rotas.filter(feriado=1)
    
    if('domingo' in mapQuery):
        rotas = rotas.filter(domingo=1)
        
    if('segunda' in mapQuery):
        rotas = rotas.filter(segunda=1)
        
    if('terca' in mapQuery):
        rotas = rotas.filter(terca=1)
        
    if('quarta' in mapQuery):
        rotas = rotas.filter(quarta=1)
        
    if('quinta' in mapQuery):
        rotas = rotas.filter(quinta=1)
        
    if('sexta' in mapQuery):
        rotas = rotas.filter(sexta=1)
        
    if('sabado' in mapQuery):
        rotas = rotas.filter(sabado=1)
        
    if('saida' in mapQuery):
        rotas = rotas.filter(horario_saida=mapQuery['saida'])    
        
    if('chegada' in mapQuery):
        rotas = rotas.filter(horario_chegada=mapQuery['chegada'])    
    
    return rotas

def getItinerariosByQuery(mapQuery):
    
    itinerarios = Itinerario.objects.all()
    
    if('id_cliente' in mapQuery
       or 'nome_cliente' in mapQuery
       or 'id_linhas' in mapQuery
       or 'nome_linha' in mapQuery
       or 'saida' in mapQuery
       or 'chegada' in mapQuery):
        novosItinerarios = []
        for rota in getRotasByQuery(mapQuery):
            novosItinerarios.extend(itinerarios.filter(id_rota=rota.id))
        itinerarios = novosItinerarios 
            
    if('id_rota' in mapQuery):
        itinerarios = itinerarios.filter(id_rota=mapQuery['id_rota'])
            
    if('sequencia' in mapQuery):
        itinerarios = itinerarios.filter(sequencia=mapQuery['sequencia'])
        
    if('horario' in mapQuery):
        itinerarios = itinerarios.filter(horario=mapQuery['horario'])
        
    if('local' in mapQuery):
        itinerarios = itinerarios.filter(local__icontains=mapQuery['local'])    
        
    return itinerarios   
        
def validaCnpj(cnpj):
    cnpj = ''.join(re.findall('\d', str(cnpj)))

    if (not cnpj) or (len(cnpj) < 14):
        return False

    # Pega apenas os 12 primeiros dígitos do CNPJ e gera os 2 dígitos que faltam
    inteiros = list(map(int, cnpj))
    novo = inteiros[:12]

    prod = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
    while len(novo) < 14:
        r = sum([x*y for (x, y) in zip(novo, prod)]) % 11
        if r > 1:
            f = 11 - r
        else:
            f = 0
        novo.append(f)
        prod.insert(0, 6)

    # Se o número gerado coincidir com o número original, é válido
    if novo == inteiros:
        return True
    return False
            
def validaCliente(newCliente):
    erros = []
    if not validaCnpj(newCliente.cnpj):
        erros.insert(len(erros), 'CNPJ Inválido!')
     
    for cliente in Cliente.objects.all():
        if cliente.id == newCliente.id:
            continue
        if cliente.cnpj == newCliente.cnpj:
            erros.insert(len(erros),'CNPJ já cadastrado!')
            break
        
    return erros