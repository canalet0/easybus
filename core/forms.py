from django import forms

from core.models import Cliente


class ClienteForm(forms.Form):
    nome = forms.CharField(required=True)
    cnpj = forms.CharField(required=False)
    email = forms.CharField(required=True)
    telefone = forms.CharField(required=True)
    
    def __init__(self, *args, **kwargs):
        super(ClienteForm, self).__init__(*args, **kwargs) # Call to ModelForm constructor
        self.fields['nome'].widget.attrs['style'] = 'width:600px;'
        self.fields['cnpj'].widget.attrs['style'] = 'width:600px;'
        self.fields['cnpj'].label= 'CNPJ*'
        self.fields['email'].widget.attrs['style'] = 'width:600px;'
        self.fields['telefone'].widget.attrs['style'] = 'width:600px;'