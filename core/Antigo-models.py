# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class Cliente(models.Model):
    id = models.BigIntegerField(primary_key=True)
    nome = models.CharField(max_length=255, blank=True, null=True)
    cnpj = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    telefone = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cliente'
        
class Usuario(models.Model):
    id = models.BigIntegerField(primary_key=True)
    user = models.OneToOneField(User)
    id_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='id_cliente')
    telefone = models.CharField(max_length=255, blank=True, null=True)
    cpf_cnpj = models.CharField(max_length=255, blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'usuario'
    
class Itinerario(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_rota = models.ForeignKey('Rota', models.DO_NOTHING, db_column='id_rota')
    local = models.CharField(max_length=255)
    sequencia = models.IntegerField()
    horario = models.CharField(max_length=255)
    latitude = models.CharField(max_length=255)
    longitude = models.CharField(max_length=255)
    parada = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'itinerario'


class Linha(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='id_cliente')
    nome_linha = models.CharField(max_length=255)
    origem = models.CharField(max_length=255)
    destino = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'linha'


class Rota(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_linha = models.ForeignKey(Linha, models.DO_NOTHING, db_column='id_linha')
    horario_saida = models.CharField(max_length=255)
    horario_chegada = models.CharField(max_length=255)
    estudante = models.IntegerField()
    segunda = models.IntegerField()
    terca = models.IntegerField()
    quarta = models.IntegerField()
    quinta = models.IntegerField()
    sexta = models.IntegerField()
    sabado = models.IntegerField()
    domingo = models.IntegerField()
    feriado = models.IntegerField()
    especiais = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'rota'

#teste