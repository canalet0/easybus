from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^loginUser/$', views.loginUser, name='loginUser'),
    url(r'^logoutUser/$', views.logoutUser, name='logoutUser'),
    url(r'^register/$', views.register, name='register'),    
    url(r'^manutCliente/$',views.listCliente, name='listCliente'),
    url(r'^manutCliente/(?P<crudOption>\w+)/$',views.manutCliente, name='manutCliente'),
    url(r'^manutCliente/(?P<crudOption>\w+)/(?P<ID>[0-9]{1,4})/$',views.manutCliente, name='manutCliente'),
    url(r'^manutLinha/$',views.listLinha, name='manutLinha'),
    url(r'^manutLinha/(?P<crudOption>\w+)/$',views.manutLinha, name='manutLinha'),
    url(r'^manutLinha/(?P<crudOption>\w+)/(?P<ID>[0-9]{1,4})/$',views.manutLinha, name='manutLinha'),
    url(r'^manutRota/$',views.listRota, name='manutRota'),
    url(r'^manutRota/(?P<crudOption>\w+)/$',views.manutRota, name='manutRota'),
    url(r'^manutRota/(?P<crudOption>\w+)/(?P<ID>[0-9]{1,4})/$',views.manutRota, name='manutRota'),
    url(r'^manutItinerario/$',views.listItinerario, name='manutItinerario'),
    url(r'^manutItinerario/(?P<crudOption>\w+)/$',views.manutItinerario, name='manutItinerario'),
    url(r'^manutItinerario/(?P<crudOption>\w+)/(?P<ID>[0-9]{1,4})/$',views.manutItinerario, name='manutItinerario'),
    url(r'^manutTrajeto/$',views.listTrajeto, name='manutTrajeto'),
        
    url(r'^(.*)/buscaClientes$',views.buscaClientes,name='buscaClientes'),
    url(r'^(.*)/buscaLinhas$',views.buscaLinhas,name='buscaLinhas'),
    url(r'^(.*)/buscaRotas$',views.buscaRotas,name='buscaRotas'),
    url(r'^(.*)/buscaItinerarios$',views.buscaItinerarios,name='buscaItinerarios'),
]