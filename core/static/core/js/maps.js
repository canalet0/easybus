var geocoder;
var map;
var marker;
var oldLat;
var oldLng;
var oldAddres;
var direcoes;
var directionsDisplay;
var directionsService;

function initializeMap() {

	$('#buscaEndereco').val($('#local').val());
	var lat = $("#latitude").val() != "" ? $("#latitude").val() : -18.8800397;
	var lng = $("#longitude").val() != "" ? $("#longitude").val() : -47.05878999999999;
	var endZoom = $("#latitude").val() != "" ? 17 : 5;
	
	var latlng = new google.maps.LatLng(lat, lng);
	var options = {
		zoom: endZoom,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	map = new google.maps.Map(document.getElementById("mapa"), options);
	
	geocoder = new google.maps.Geocoder();
	
	marker = new google.maps.Marker({
		map: map,
		draggable: true,
	});
	
	marker.setPosition(latlng);
}

function carregarNoMapa(endereco) {
	
	geocoder.geocode({ 'address': endereco + ', Brasil', 'region': 'BR' }, function (results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) {
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
	
				/*$('#buscaEndereco').val(results[0].formatted_address);
				$('#latitude').val(latitude);
               	$('#longitude').val(longitude);*/
	
				var location = new google.maps.LatLng(latitude, longitude);
				marker.setPosition(location);
				map.setCenter(location);
				map.setZoom(17);
			}
		}
	})
}

function setLocation(){
	$('#local').val($('#buscaEndereco').val());
	$('#latitude').val(marker.getPosition().lat());
   	$('#longitude').val(marker.getPosition().lng());
}

function initializeMapComponents() {
	
	initializeMap();
	
	$("#buscaEndereco").blur(function() {
		if($(this).val() != "")
			carregarNoMapa($(this).val());
	});
	
	google.maps.event.addListener(marker, 'drag', function () {
		geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {  
					$('#buscaEndereco').val(results[0].formatted_address);
				}
			}
		});
	});
	
	$("#buscaEndereco").autocomplete({
        source: function (request, response) {
            geocoder.geocode({ 'address': request.term + ', Brasil', 'region': 'BR' }, function (results, status) {
                response($.map(results, function (item) {
                    return {
                        label: item.formatted_address,
                        value: item.formatted_address,
                        latitude: item.geometry.location.lat(),
                        longitude: item.geometry.location.lng()
                    }
                }));
            })
        },
        select: function (event, ui) {
            var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
            marker.setPosition(location);
            map.setCenter(location);
            map.setZoom(16);
            
        },
        messages: {
            noResults: '',
            results: function() {}
        }
    });
}

function buscaTrajeto(idRota) {
	 
	var json = "{"
	json += "\"id_rota\":\"" + idRota + "\"";
	json += "}";

	$.ajax({
		url : "buscaItinerarios",
		type : "POST",
		dataType : "json",
		data : {
            client_response : json,
            csrfmiddlewaretoken : $("[name='csrfmiddlewaretoken']").val()
		},
		success : function(retorno) {
			$('#mapModal').modal('show');
			direcoes = retorno;
		},
		error : function(xhr,errmsg,err) {
			alert(xhr.status + ": " + xhr.responseText);
		}
	});
}

function addWayPoints(){
	var waypts = [];

	if(direcoes.length > 2){	
		for (i = 1; i < direcoes.length -1; i++) {
			waypts.push({
				location: latlng = new google.maps.LatLng(direcoes[i][1], direcoes[i][2]),
				stopover: true
			});
		}
	}
	
	return waypts;
}

function addMarkers(map) {    
	var sequenciaParada = 1;
	
    for (i = 0; i < direcoes.length; i++) {
    	var isParada = direcoes[i][3];
    	
    	if(!isParada)
    		continue;
    	
    	var html = '<b>Parada: ' + sequenciaParada + '</b> <br/> Horário: ' + direcoes[i][5] +   '<br/>' + direcoes[i][4];
    	
	    var marker = new google.maps.Marker({
	      position: latlng = new google.maps.LatLng(direcoes[i][1], direcoes[i][2]),
	      map: map,
	      title:'Parada: ' + sequenciaParada,
	      zIndex: Math.round(latlng.lat() * -100000) << 5
	    });
	    
	    marker.info = new google.maps.InfoWindow({
	    	content: html
	    });
	    
	    google.maps.event.addListener(marker, 'click', function() {  
	        var marker_map = this.getMap();
	        this.info.open(marker_map,this);
	    });
	    
	    sequenciaParada++;
    }
}

function initializeMapMarkers() {
	
	directionsDisplay = new google.maps.DirectionsRenderer({
      suppressMarkers: true
    });
	directionsService = new google.maps.DirectionsService();
	
	var latlng = new google.maps.LatLng(direcoes[0][1], direcoes[0][2]);
	var latlngDestiny = new google.maps.LatLng(direcoes[direcoes.length-1][1], direcoes[direcoes.length-1][2]);
	
	var options = {
        zoom: 16,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
	
	var map = new google.maps.Map(document.getElementById("mapa"), options);
	directionsDisplay.setMap(map);
	
	var request = {
		origin: latlng,
		destination: latlngDestiny,
		waypoints: addWayPoints(),
		travelMode: google.maps.TravelMode.DRIVING
	};
 
	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(result);
		}
	});
	
	addMarkers(map);
}