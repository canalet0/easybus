function mascaraMutuario(o,f){
    v_obj=o;
    v_fun=f;
    setTimeout('execmascara()',1);
}
 
function execmascara(){
    v_obj.value=v_fun(v_obj.value);
}
 
function mascaraCpf(v){
 
	v=onlyNumbers(v);
 
    //Coloca um ponto entre o terceiro e o quarto dígitos
    v=v.replace(/(\d{3})(\d)/,"$1.$2");
 
    //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    v=v.replace(/(\d{3})(\d)/,"$1.$2");
 
    //Coloca um hífen entre o terceiro e o quarto dígitos
    v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

    return v;
}
        
function mascaraCnpj(v){

	v=onlyNumbers(v);
	
    //Coloca ponto entre o segundo e o terceiro dígitos
    v=v.replace(/^(\d{2})(\d)/,"$1.$2");
 
    //Coloca ponto entre o quinto e o sexto dígitos
    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3");
 
    //Coloca uma barra entre o oitavo e o nono dígitos
    v=v.replace(/\.(\d{3})(\d)/,".$1/$2");
 
    //Coloca um hífen depois do bloco de quatro dígitos
    v=v.replace(/(\d{4})(\d)/,"$1-$2");
 
    return v;
}

function mascaraTelefone(v){

    v=onlyNumbers(v);
    
    v=v.replace(/^(\d{2})(\d)/,"($1) $2");
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");
 
    return v;
}

function onlyNumbers(v){
	return v=v.replace(/\D/g,"");
}