var searchUrl = "";

function atualizaUrlBusca(searchUrlPar){
	searchUrl = searchUrlPar;
}

$(document).ready(function() {		
	$("#searchFilter").keyup(function () {
	    var searchData = this.value.toUpperCase().split(" ");
	    var tableData = $("#manutListTable").find("tbody").find("tr");
	    if (this.value == "") {
	        tableData.show();
	        return;
	    }
	    tableData.hide();

	    tableData.filter(function (i, v) {
	        var $tableLine = $(this);
	        for (var i = 0; i < searchData.length; ++i) {
	            if ($tableLine.text().toUpperCase().indexOf(searchData[i]) > -1) {
	                return true;
	            }
	        }
	        return false;
	    }).show();
	});
});

function setSearchData() {	
	var searchParameters = "\"tipoRetorno\":\"table\"";
	
	if($("#searchIdCliente").length > 0 && $('#searchIdCliente').val() != ""){
		searchParameters += ",\"id_cliente\":\"" + $('#searchIdCliente').val() + "\"";
	}

	if($("#searchNomeCliente").length > 0 && $('#searchNomeCliente').val() != ""){
		searchParameters += ",\"nome_cliente\":\"" + $('#searchNomeCliente').val() + "\"";
	}
	
	if($("#searchCnpjCliente").length > 0 && $('#searchCnpjCliente').val() != ""){
		searchParameters += ",\"cnpj_cliente\":\"" + $('#searchCnpj').val() + "\"";
	}
	
	if($("#searchIdLinha").length > 0 && $('#searchIdLinha').val() != ""){
		searchParameters += ",\"id_linha\":\"" + $('#searchIdLinha').val() + "\"";
	}
	
	if($("#searchNomeLinha").length > 0 && $('#searchNomeLinha').val() != ""){
		searchParameters += ",\"nome_linha\":\"" + $('#searchNomeLinha').val() + "\"";
	}
	
	if($("#searchIdRota").length > 0 && $('#searchIdRota').val() != ""){
		searchParameters += ",\"id_rota\":\"" + $('#searchIdRota').val() + "\"";
	}
	
	if($("#saida").length > 0 && $('#saida').val() != ""){
		searchParameters += ",\"saida\":\"" + $('#saida').val() + "\"";
	}
	
	if($("#chegada").length > 0 && $('#chegada').val() != ""){
		searchParameters += ",\"chegada\":\"" + $('#chegada').val() + "\"";
	}

	if($("#estudantes").length > 0 && $('#estudantes').is(":checked"))
		searchParameters += ",\"estudantes\":\"" + $('#estudantes').is(":checked") + "\"";
	
	if($("#especiais").length > 0 && $('#especiais').is(":checked"))	
		searchParameters += ",\"especiais\":\"" + $('#especiais').is(":checked") + "\"";
		
	if($("#domingo").length > 0 && $('#domingo').is(":checked"))
		searchParameters += ",\"domingo\":\"" + $('#domingo').is(":checked") + "\"";
		
	if($("#segunda").length > 0 && $('#segunda').is(":checked"))
		searchParameters += ",\"segunda\":\"" + $('#segunda').is(":checked") + "\"";
		
	if($("#terca").length > 0 && $('#terca').is(":checked"))
		searchParameters += ",\"terca\":\"" + $('#terca').is(":checked") + "\"";
		
	if($("#quarta").length > 0 && $('#quarta').is(":checked"))
		searchParameters += ",\"quarta\":\"" + $('#quarta').is(":checked") + "\"";
		
	if($("#quinta").length > 0 && $('#quinta').is(":checked"))
		searchParameters += ",\"quinta\":\"" + $('#quinta').is(":checked") + "\"";
		
	if($("#sexta").length > 0 && $('#sexta').is(":checked"))			
		searchParameters += ",\"sexta\":\"" + $('#sexta').is(":checked") + "\"";
		
	if($("#sabado").length > 0 && $('#sabado').is(":checked"))
		searchParameters += ",\"sabado\":\"" + $('#sabado').is(":checked") + "\"";
	
	if($("#feriado").length > 0 && $('#feriado').is(":checked"))
		searchParameters += ",\"feriado\":\"" + $('#feriado').is(":checked") + "\"";
	
	if($("#searchSequenciaItinerario").length > 0 && $('#searchSequenciaItinerario').val() != ""){
		searchParameters += ",\"sequencia\":\"" + $('#searchSequenciaItinerario').val() + "\"";
	}
	
	if($("#horarioItinerario").length > 0 && $('#horarioItinerario').val() != ""){
		searchParameters += ",\"horario\":\"" + $('#horarioItinerario').val() + "\"";
	}
	
	if($("#searchLocal").length > 0 && $('#searchLocal').val() != ""){
		searchParameters += ",\"local\":\"" + $('#searchLocal').val() + "\"";
	}
	
	searchData(searchParameters,searchUrl,'table');
}


function openAdvSearch() {
	$('#searchIdCliente').val("");
	$('#searchNomeCliente').val("");
	$('#searchIdLinha').val("");
	$('#searchNomeLinha').val("");
	$('#searchIdRota').val("");
	$('#saida').val("");
	$('#chegada').val("");
	$('#estudantes').prop('checked',false);
	$('#especiais').prop('checked',false);
	$('#domingo').prop('checked',false);
	$('#segunda').prop('checked',false);
	$('#terca').prop('checked',false);
	$('#quarta').prop('checked',false);
	$('#quinta').prop('checked',false);
	$('#sexta').prop('checked',false);
	$('#sabado').prop('checked',false);
	$('#feriado').prop('checked',false);
	$('#searchSequenciaItinerario').val("");
	$('#horarioItinerario').val("");
	$('#searchLocal').val("");
}