DROP TABLE IF EXISTS "cliente";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "cliente" (
  "id" bigint(10) NOT NULL,
  "nome" varchar(255) DEFAULT NULL,
  "cnpj" varchar(255) DEFAULT NULL,
  "email" varchar(255) DEFAULT NULL,
  "telefone" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table "cliente"
--

LOCK TABLES "cliente" WRITE;
/*!40000 ALTER TABLE "cliente" DISABLE KEYS */;
INSERT INTO "cliente" VALUES (11,'Ozelame','30.236.883/0001-02','ozelame@gmail.com','(54) 9999-9999'),(12,'Cidade de Farroupilha','81.055.144/0001-47','farroupilha@gmail.com','(54) 9999-8888'),(13,'dasdasdas','22.652.846/0001-49','teste@teste.com.br','(12) 31231-2312'),(18,'a','64.512.115/0001-48','teste@teste.com.br','(12) 312-3123'),(24,'asdkopasdkpoasaaa','70.180.485/0001-28','teste@teste.com.br','(54) 54545-4545');
/*!40000 ALTER TABLE "cliente" ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS "itinerario";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "itinerario" (
  "id" bigint(10) NOT NULL,
  "id_rota" bigint(10) NOT NULL,
  "local" varchar(255) NOT NULL,
  "sequencia" bigint(11) NOT NULL,
  "horario" varchar(255) NOT NULL,
  "latitude" varchar(255) NOT NULL,
  "longitude" varchar(255) NOT NULL,
  "parada" boolean NOT NULL,
  PRIMARY KEY ("id"),
  KEY "fk_itinerario_rota" ("id_rota"),
  CONSTRAINT "fk_itinerario_rota" FOREIGN KEY ("id_rota") REFERENCES "rota" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table "itinerario"
--

LOCK TABLES "itinerario" WRITE;
/*!40000 ALTER TABLE "itinerario" DISABLE KEYS */;
INSERT INTO "itinerario" VALUES (1,1,'R. Jos� Maioli - Monte Pasqual, Farroupilha - RS, 95180-000, Brasil',1,'00:00','-29.2226384','-51.31467520000001',1),(3,1,'R. Santa Maria - Monte Pasqual, Farroupilha - RS, 95180-000, Brasil',2,'00:18','-29.2225096','-51.31611499999997',1),(4,1,'R. dos Jacarand�s - Cinquenten�rio, Caxias do Sul - RS, 95012-280, Brasil',4,'00:30','-29.169705','-51.211050099999966',1),(5,1,'R. S�o Crist�v�o, 165 - Forqueta, Caxias do Sul - RS, 95115-490, Brasil',3,'00:15','-29.2126862','-51.28110570000001',0);
/*!40000 ALTER TABLE "itinerario" ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table "linha"
--

DROP TABLE IF EXISTS "linha";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "linha" (
  "id" bigint(10) NOT NULL,
  "id_cliente" bigint(10) NOT NULL,
  "nome_linha" varchar(255) NOT NULL,
  "origem" varchar(255) NOT NULL,
  "destino" varchar(255) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "fk_linha_cliente" ("id_cliente"),
  CONSTRAINT "fk_linha_cliente" FOREIGN KEY ("id_cliente") REFERENCES "cliente" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table "linha"
--

LOCK TABLES "linha" WRITE;
/*!40000 ALTER TABLE "linha" DISABLE KEYS */;
INSERT INTO "linha" VALUES (1,11,'Farroupilha x Caxias do Sul','Farroupilha','Caxias do Sul'),(2,11,'Farroupilha x Bento Gon�alves','teste','Bento Gon�alves'),(3,12,'linha 1 cliente 12','a','b'),(4,12,'linha 2 cliente 12','C','D');
/*!40000 ALTER TABLE "linha" ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS "rota";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "rota" (
  "id" bigint(10) NOT NULL,
  "id_linha" bigint(10) NOT NULL,
  "horario_saida" varchar(255) NOT NULL,
  "horario_chegada" varchar(255) NOT NULL,
  "estudante" boolean NOT NULL,
  "segunda" boolean NOT NULL,
  "terca" boolean NOT NULL,
  "quarta" boolean NOT NULL,
  "quinta" boolean NOT NULL,
  "sexta" boolean NOT NULL,
  "sabado" boolean NOT NULL,
  "domingo" boolean NOT NULL,
  "feriado" boolean NOT NULL,
  "especiais" boolean NOT NULL,
  PRIMARY KEY ("id"),
  KEY "fk_rota_linha" ("id_linha"),
  CONSTRAINT "fk_rota_linha" FOREIGN KEY ("id_linha") REFERENCES "linha" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table "rota"
--

LOCK TABLES "rota" WRITE;
/*!40000 ALTER TABLE "rota" DISABLE KEYS */;
INSERT INTO "rota" VALUES (1,1,'00:00','01:00',0,0,0,0,0,0,0,0,0,1),(2,1,'01:00','02:00',0,0,0,0,0,0,0,0,0,0),(3,1,'02:00','03:00',1,0,0,0,0,0,0,1,0,1),(4,3,'00:00','01:00',0,0,0,0,0,0,0,1,0,0),(5,3,'02:00','03:00',0,0,0,0,0,0,0,1,0,0),(7,3,'05:00','06:00',1,0,0,0,0,0,0,1,0,1),(8,2,'00:00','18:44',0,1,0,0,0,0,0,1,0,0);
/*!40000 ALTER TABLE "rota" ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table "usuario"
--

DROP TABLE IF EXISTS "usuario";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "usuario" (
  "user_id" bigint(10) NOT NULL,
  "id_cliente" bigint(10) DEFAULT NULL,
  "telefone" varchar(255) NOT NULL,
  "cpf_cnpj" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("telefone"),
  UNIQUE KEY "user_id_UNIQUE" ("user_id"),
  KEY "fk_usuario_1_idx" ("id_cliente"),
  CONSTRAINT "fk_usuario_1" FOREIGN KEY ("id_cliente") REFERENCES "cliente" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table "usuario"
--

LOCK TABLES "usuario" WRITE;
/*!40000 ALTER TABLE "usuario" DISABLE KEYS */;
/*!40000 ALTER TABLE "usuario" ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-01 22:54:18